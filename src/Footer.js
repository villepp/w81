import React from 'react';

function Footer() {
  return (
    <footer>
      <p>© 2024 Random Quotes. All rights reserved.</p>
    </footer>
  );
}

export default Footer;
