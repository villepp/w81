import React from "react";
import { render, fireEvent } from "@testing-library/react";
import QuoteManager from "../Components/QuoteManager";

test("renders QuoteManager without crashing", () => {
  render(<QuoteManager />);
});

test("displays loading text initially", () => {
  const { queryByText } = render(<QuoteManager />);
  expect(queryByText(/Loading quote.../i)).toBeInTheDocument();
});

test("renders input fields correctly", () => {
  const { getByPlaceholderText } = render(<QuoteManager />);
  expect(getByPlaceholderText(/Enter quote/i)).toBeInTheDocument();
  expect(getByPlaceholderText(/Author/i)).toBeInTheDocument();
});

test("updates input fields when text is entered", () => {
  const { getByPlaceholderText } = render(<QuoteManager />);
  const quoteInput = getByPlaceholderText(/Enter quote/i);
  const authorInput = getByPlaceholderText(/Author/i);

  fireEvent.change(quoteInput, { target: { value: "Test Quote" } });
  fireEvent.change(authorInput, { target: { value: "Test Author" } });

  expect(quoteInput.value).toBe("Test Quote");
  expect(authorInput.value).toBe("Test Author");
});
