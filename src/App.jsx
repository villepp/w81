import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import NavBar from './Components/NavBar';
import QuoteManager from './Components/QuoteManager';
import AboutPage from './Components/AboutPage';
import ContactPage from './Components/ContactPage';
import "./App.css";

function App() {
  return (
    <Router>
      <div>
        <NavBar currentPath={window.location.pathname} />
        <Routes>
          <Route path="/" element={<QuoteManager />} />
          <Route path="/about" element={<AboutPage />} />
          <Route path="/contact" element={<ContactPage />} />
        </Routes>
      </div>
    </Router>
  );
}


export default App;
