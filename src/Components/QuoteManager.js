import React, { useState, useEffect } from "react";
import { ref, push, set, onValue } from "firebase/database";
import StartFirebase from "./Firebase";

function QuoteManager() {
  const [quote, setQuote] = useState("");
  const [author, setAuthor] = useState("");
  const [displayQuote, setDisplayQuote] = useState("Loading quote...");

  const handleSubmit = (event) => {
    event.preventDefault();
    const database = StartFirebase();
    const quotesRef = ref(database, "quotes");
    const newQuoteRef = push(quotesRef);
    set(newQuoteRef, { quoteText: quote, author: author })
      .then(() => {
        alert("Quote added successfully!");
        setQuote("");
        setAuthor("");
      })
      .catch((error) => {
        alert("Failed to add quote: " + error.message);
      });
  };

  const getRandomQuote = () => {
    const database = StartFirebase();
    const quotesRef = ref(database, "quotes");
    onValue(
      quotesRef,
      (snapshot) => {
        const data = snapshot.val();
        if (data) {
          const quoteIds = Object.keys(data);
          const randomQuoteId =
            quoteIds[Math.floor(Math.random() * quoteIds.length)];
          const randomQuote = data[randomQuoteId];
          setDisplayQuote(`${randomQuote.quoteText} - ${randomQuote.author}`);
        } else {
          setDisplayQuote("No quotes available.");
        }
      },
      {
        onlyOnce: true,
      }
    );
  };

  useEffect(() => {
    getRandomQuote();
  }, []);

  return (
    <div>
      <div className="container quote-display-area">
        <p>{displayQuote}</p>
        <button onClick={getRandomQuote}>Get Random Quote</button>
      </div>
      <div className="container quote-form">
        <form onSubmit={handleSubmit}>
          <input
            type="text"
            value={quote}
            onChange={(e) => setQuote(e.target.value)}
            placeholder="Enter quote"
            required
          />
          <input
            type="text"
            value={author}
            onChange={(e) => setAuthor(e.target.value)}
            placeholder="Author"
            required
          />
          <button type="submit">Add Quote</button>
        </form>
      </div>
    </div>
  );
}

export default QuoteManager;
