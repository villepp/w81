import { initializeApp } from "firebase/app";
import { getDatabase } from "firebase/database";

function StartFirebase() {
  const firebaseConfig = {
    apiKey: "AIzaSyAkTa680R29nf011v3e28KPjKm7yLzfmMo",
    authDomain: "randomquote-df08a.firebaseapp.com",
    databaseURL:
      "https://randomquote-df08a-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "randomquote-df08a",
    storageBucket: "randomquote-df08a.appspot.com",
    messagingSenderId: "1008221395326",
    appId: "1:1008221395326:web:49749c80d9e17e9bdbbb2f",
    measurementId: "G-SNGVSCDYNY",
  };

  const app = initializeApp(firebaseConfig);
  return getDatabase(app);
}

export default StartFirebase;
