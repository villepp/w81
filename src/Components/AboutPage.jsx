import React from 'react';

function AboutPage() {
  return (
    <div className="about-container">
      <h1>About Random Quote Generator</h1>
      <p>Welcome to the Random Quote Generator! This application showcases React and Firebase by providing a dynamic web experience where you can discover and share inspirational quotes.</p>
      <h2>How It Works</h2>
      <p><strong>Get Quotes:</strong> Click "Get Random Quote" to fetch a random quote from our Firebase database, which stores a curated collection of motivational sayings from various authors.</p>
      <p><strong>Add Quotes:</strong> Contribute new quotes by entering the text and author's name, then submit to add your quote to the database. It might just inspire someone else when they visit!</p>
      <h2>Technologies Used</h2>
      <p>The app is built using:</p>
      <ul>
        <li><strong>React.js:</strong> For creating an interactive user interface.</li>
        <li><strong>Firebase:</strong> Provides a real-time database for storing and retrieving quotes.</li>
        <li><strong>React Router:</strong> Manages navigation between different sections of the app.</li>
      </ul>
    </div>
  );
}

export default AboutPage;
