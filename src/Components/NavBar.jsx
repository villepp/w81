import React from 'react';
import { NavLink } from 'react-router-dom';

function NavBar({ currentPath }) {
  return (
    <nav className="nav-bar">
      <ul>
        <li>
          <NavLink to="/" exact className={currentPath === '/' ? 'active' : ''}>
            Home
          </NavLink>
        </li>
        <li>
          <NavLink to="/about" className={currentPath === '/about' ? 'active' : ''}>
            About
          </NavLink>
        </li>
        <li>
          <NavLink to="/contact" className={currentPath === '/contact' ? 'active' : ''}>
            Contact
          </NavLink>
        </li>
      </ul>
    </nav>
  );
}

export default NavBar;
