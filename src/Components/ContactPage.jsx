import React from 'react';

function ContactPage() {
  return (
    <div className="contact-container">
      <h1>Contact Us</h1>
      <p>Reach out to us! Whether you have questions about the app, want to leave feedback, or simply say hello, we're here to listen.</p>
      <ul>
        <li>Email: <a href="mailto:support@example.com">support@example.com</a></li>
        <li>Twitter: <a href="https://twitter.com/example">@example</a></li>
      </ul>
    </div>
  );
}

export default ContactPage;
